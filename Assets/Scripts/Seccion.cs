﻿using UnityEngine;
using System.Collections;

public class Seccion : MonoBehaviour {


    #region ATRIBUTOS
    [SerializeField]
    private GameObject[] listaBoxes;
    #endregion

    #region PROPIEDADES
    public GameObject[] ListaBoxes
    {
        get
        {
            return listaBoxes;
        }

        set
        {
            listaBoxes = value;
        }
    }
    #endregion

    #region METODOS UNITY

    #endregion

    #region METODOS

    #endregion
}
