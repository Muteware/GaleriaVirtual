﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;
using System;

public class LectorJSONv2 : MonoBehaviour
{
    #region ATRIBUTOS
    //Objetos JSON
    private string JsonLeido { set; get; }
    private JSONNode JSONParseado { set; get; }
    private string DireccionJSON { set; get; }
    internal DatosCuadro[] ListaCuadrosLeidos { set; get; }
    private bool TengoQueLeerCuadros { set; get; }
    internal bool TengoListaCuadrosLeidos { set; get; }

    //Estados
    private bool TengoQueParsear { set; get; }
    internal bool TengoQueProcesar { set; get; }

    internal int PisoParaCargar { set; get; }
    #endregion

    #region PROPIEDADES
    /*
    public JSONNode JSONParseado
    {
        get
        {
            return jsonParseado;
        }

        set
        {
            jsonParseado = value;
        }
    }


    public string DireccionJSON
    {
        get
        {
            return direccionJSON;
        }

        set
        {
            direccionJSON = value;
        }
    }

    public DatosCuadro[] ListaCuadrosLeidos
    {
        get
        {
            return listaCuadrosLeidos;
        }

        set
        {
            listaCuadrosLeidos = value;
        }
    }

    public bool TengoListaCuadrosLeidos
    {
        get
        {
            return tengoListaCuadrosLeidos;
        }

        set
        {
            tengoListaCuadrosLeidos = value;
        }
    }

    public bool TengoQueLeerCuadros
    {
        get
        {
            return tengoQueLeerCuadros;
        }

        set
        {
            tengoQueLeerCuadros = value;
        }
    }

    public bool TengoQueParsear
    {
        get
        {
            return tengoQueParsear;
        }

        set
        {
            tengoQueParsear = value;
        }
    }

    public bool TengoQueProcesar
    {
        get
        {
            return tengoQueProcesar;
        }

        set
        {
            tengoQueProcesar = value;
        }
    }

    public string JsonLeido
    {
        get
        {
            return jsonLeido;
        }

        set
        {
            jsonLeido = value;
        }
    }
    */
    #endregion

    #region METODOS UNITY
    void Start()
    {
        //Inicializo todos los estados en false
        this.InicializarEstados();

        //Guardo la direccion
        this.DireccionJSON = "entregajson.php";

#if UNITY_EDITOR    
        //Guardo la direccion SOLO PARA EL EDITOR
        this.DireccionJSON = "http://www.muteware.com/galeria/entregable/final/entregajson.php";
#endif

        // Comineza a leer el JSON.
        this.ComenzarCorutinaJSON();

        this.PisoParaCargar = 0;
    }

    void Update()
    {
        if (this.TengoQueParsear)
        {
            this.TengoQueParsear = false;

            this.ParsearJSON();
        }

        if (this.TengoQueProcesar)
        {
            this.TengoQueProcesar = false;

            if (this.JSONParseado != null)
            {
                this.ListaCuadrosLeidos = this.ProcesarDatosJSON(this.PisoParaCargar);
            }
            else
            {
                this.TengoQueProcesar = true;
            }
        }
    }
    #endregion

    #region METODOS
    /// <summary>
    /// Este metodo va a inicializar los estados de las acciones a realizar
    /// </summary>
    private void InicializarEstados()
    {
        this.TengoQueParsear = false;
        this.TengoQueProcesar = false;
        this.TengoListaCuadrosLeidos = false;
        this.TengoQueLeerCuadros = false;
    }

    /// <summary>
    /// Este metodo va a invocar a la Corutina que lee el JSON
    /// </summary>
    public void ComenzarCorutinaJSON()
    {
        StartCoroutine("LeerJSON");
    }

    /// <summary>
    /// Este metodo se encarga de leer el JSON de la galeria con todos los datos de los cuadros.
    /// </summary>
    IEnumerator LeerJSON()
    {
        //Leo el webService.
        WWW www = new WWW(this.DireccionJSON);

        //Espero a que termine
        yield return www;

        //GameObject.Find("Text").GetComponent<Text>().text = "Termino el Yield \n";

        //Si no devuelve error.
        if (www.error == null)
        {
            //Guardo lo leido
            this.JsonLeido = www.text;

            //Indico que ya puedo parsear
            this.TengoQueParsear = true;
            //GameObject.Find("Text").GetComponent<Text>().text += "Termine Leer JSON \n";
        }
        else
        {
            Debug.Log("WTF: " + www.error.ToString());
        }

        www.Dispose();
        www = null;
    }

    /// <summary>
    /// Este metodo va a parsear el JSON leido
    /// </summary>
    private void ParsearJSON()
    {
        //Parseo el JSON
        this.JSONParseado = JSON.Parse(this.JsonLeido);

        //GameObject.Find("Text").GetComponent<Text>().text += "Termine parsear JSON \n";
        this.TengoQueProcesar = true;
    }

    /// <summary>
	/// El metodo va a usar los datos leidos desde los JSON para crear los Objetos que luego voy a instanciar.
    /// Este metodo recibe el piso que se quiere parsear
	/// </summary>
	public DatosCuadro[] ProcesarDatosJSON(int piso)
    {
        int cuadrosCargadoEnElPiso = 0;

        //Creo una variable temporal
        var listaCuadrosLeidosTemp = new ArrayList();
        

        //Para cada uno de los resultados.
        for (int i = 0; i < this.JSONParseado.Count; i++)
        {
            if (this.JSONParseado[i]["piso"].AsInt == piso)
            {
                if (cuadrosCargadoEnElPiso <= 300)
                {
                    //Creo un nuevo objeto con los datos que me interesan del JSON.
                    DatosCuadro unObjeto = new DatosCuadro(this.JSONParseado[i]["idobra"].AsInt,
                                                    this.JSONParseado[i]["nombreobra"].Value,
                                                    this.JSONParseado[i]["descripcion"].Value,
                                                    this.JSONParseado[i]["nombre"].Value + " " + this.JSONParseado[i]["apellido"].Value,
                                                    this.JSONParseado[i]["precio"].AsFloat,
                                                    this.JSONParseado[i]["imagenLink"].Value,
                                                    this.JSONParseado[i]["x"].AsFloat / 100, //Para pasar de Centimetros a Metros 
                                                    this.JSONParseado[i]["y"].AsFloat / 100, //Para pasar de Centimetros a Metros
                                                    this.JSONParseado[i]["piso"].AsInt,
                                                    this.JSONParseado[i]["box"].AsInt,
                                                    this.JSONParseado[i]["cuadro"].AsInt,
                                                    this.JSONParseado[i]["mail"].Value,
                                                    this.JSONParseado[i]["baja"].AsInt);

                    //Indico que cargue un cuadro
                    cuadrosCargadoEnElPiso += 1;

                    //Agrego el nuevo objeto en la lista de los objetos serializados.
                    listaCuadrosLeidosTemp.Add(unObjeto);
                    // listaCuadrosLeidos[i] = unObjeto;

                    //GameObject.Find("Text").GetComponent<Text>().text += unObjeto.ToString() + "\n";
                }
            }
        }

        //Inicializo la lista de cuadrosLeidos
        DatosCuadro[] listaCuadrosLeidos = new DatosCuadro[listaCuadrosLeidosTemp.Count];

        //Los guardo
        for (int i = 0; i < listaCuadrosLeidosTemp.Count; i++)
        {
            listaCuadrosLeidos[i] = (DatosCuadro)listaCuadrosLeidosTemp[i];
        }

        //Indico que tengo la lista de cuadros leidos
        this.TengoListaCuadrosLeidos = true;


        //Devuelvo el array de cuadros leidos
        return listaCuadrosLeidos;
    }
    #endregion
}

