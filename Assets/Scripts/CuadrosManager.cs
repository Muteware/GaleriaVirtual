﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CuadrosManager : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField] private Material materialCuadro;
    [SerializeField] private Mesh meshCuadro;
    [SerializeField] private string linkCarrito;

    private GameObject[] ListaBoxes { set; get; }
    private LectorJSONv2 LectorJSON { set; get; }
    private DatosCuadro[] ListaCuadrosLeidos { set; get; }
    private GameObject[] ListaCuadrosCargados { set; get; }

    private bool TengoQueCargarInformacion { set; get; }
    private bool TengoQueCargarTexturas { set; get; }
    private bool TengoQueCargarImagenes { set; get; }
    internal bool TermineCargarImagenes { set; get; }
    private int CantidadCuadrosCargados { set; get; }

    private GameObject[] ColaCargaTexturas { set; get; }

    #endregion

    #region PROPIEDADES
    public Material MaterialCuadro
    {
        get
        {
            return materialCuadro;
        }

        set
        {
            materialCuadro = value;
        }
    }

    public Mesh MeshCuadro
    {
        get
        {
            return meshCuadro;
        }

        set
        {
            meshCuadro = value;
        }
    }

    public string LinkCarrito
    {
        get
        {
            return linkCarrito;
        }

        set
        {
            linkCarrito = value;
        }
    }
    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start ()
    {
        this.InicializarListas();

        //Guardo todos los boxes que tengo en la lista
        this.ObtenerLosBoxes();
        
        //Creo un nuevo lector JSON. Lee automaticamente
        this.LectorJSON = this.GetComponent<LectorJSONv2>();

        this.CantidadCuadrosCargados = 0;

        this.ColaCargaTexturas = new GameObject[301];
    }
    void Update()
    {
        if (this.LectorJSON.TengoListaCuadrosLeidos)
        {

            //this.ListaCuadrosLeidos = new DatosCuadro[this.LectorJSON.ListaCuadrosLeidos.Length];
            this.ListaCuadrosLeidos = this.LectorJSON.ListaCuadrosLeidos;

            this.LectorJSON.TengoListaCuadrosLeidos = false;
            this.TengoQueCargarInformacion = true;
        }
        
        if(this.TengoQueCargarInformacion)
        {
            this.CargarInformacionCuadros();
        }
    }

    

    #endregion

    #region METODOS

    /// <summary>
    /// Este metodo va a cargar la informacion de los cuadros leida en cada cuadro
    /// </summary>
    public void CargarInformacionCuadros()
    {
        //Guardo la cantidad de cuadros que tengo que cargar
        int cantidadCuadrosParaCargar = this.ListaCuadrosLeidos.Length;

        if(this.ListaCuadrosCargados == null)
            //Genero el espacio que necesito para cargar los cuadros
            this.ListaCuadrosCargados = new GameObject[cantidadCuadrosParaCargar];

        if (this.CantidadCuadrosCargados < cantidadCuadrosParaCargar && this.ListaCuadrosLeidos[this.CantidadCuadrosCargados] != null )
        {
            //GameObject.Find("Text").GetComponent<Text>().text += "Entre a Cargar Informacion " + this.CantidadCuadrosCargados + " \n";

            //Obtengo el cuadro fisico, segun el dato del box y del cuadro
            GameObject cuadro = this.ListaBoxes[this.ListaCuadrosLeidos[this.CantidadCuadrosCargados].NumeroBox - 1].GetComponent<Boxes>().ListaCuadros[this.ListaCuadrosLeidos[this.CantidadCuadrosCargados].NumeroCuadro - 1];

            //Le asigno una nueva textura
            cuadro.GetComponent<Cuadro>().NuevaTextura = new Texture2D(256, 256);

            //Guardo los datos de lo leido del JSON el cuadro
            cuadro.GetComponent<Cuadro>().GuardarDatos(this.ListaCuadrosLeidos[this.CantidadCuadrosCargados]);

            //Establezco el nuevo tamanio del cuadro
            cuadro.GetComponent<Cuadro>().RedimensionarCuadro();

            //Indico que ya puede cargar la textura
            cuadro.GetComponent<Cuadro>().PuedoCargarTextura = true;

            //Agrego al cuadro a la lista de cuadros cargados
            this.ListaCuadrosCargados[this.CantidadCuadrosCargados] = cuadro;

            this.CantidadCuadrosCargados += 1;       
        }
        else
        {
            this.TengoQueCargarInformacion = false;
            this.TengoQueCargarTexturas = true;
            this.CantidadCuadrosCargados = 0;
        }
    }

    /// <summary>
    /// Este metodo va a guardar la lista de Boxes ordenados por su posicion
    /// </summary>
    private void ObtenerLosBoxes()
    {
        //Indico cuantas boxes voy a tener.
        this.ListaBoxes = new GameObject[GameObject.FindGameObjectsWithTag("Box").Length];

        //Recorro la lista de todos los boxes buscados y los agrego a la lista.
        foreach (GameObject unBox in GameObject.FindGameObjectsWithTag("Box"))
        {
            //Si el box que agarro es del lado izquierdo
            if (unBox.GetComponent<Boxes>().EsLadoIzquierdo)
            {
                //Le sumo 100 al dato de su posicion en la lista de Boxes
                unBox.GetComponent<Boxes>().Posicion += 100;
            }

            //Obtengo la posicion del Box
            int posicion = unBox.GetComponent<Boxes>().Posicion;

            //Guardo el box en la posicion que le fue asignada.
            this.ListaBoxes[posicion] = unBox;
        }
    }

    /// <summary>
    /// Este metodo va a dejar en blanco todos los canvas de los cuadros
    /// </summary>
    public void ReiniciarCanvasCuadros()
    {
        this.InicializarListas();

        //Obtengo todos los cuadros fisicos
        var cuadros = GameObject.FindGameObjectsWithTag("Cuadro");

        //A cada uno le asigno el canvas blanco
        foreach (GameObject unCuadro in cuadros)
        {
            //Le asigno una nueva textura
            unCuadro.GetComponent<MeshRenderer>().material = this.MaterialCuadro;
        }
    }

    /// <summary>
    /// Este metodo va a inicializar todas las listas en cero
    /// </summary>
    private void InicializarListas()
    {
        this.ListaCuadrosLeidos = null;
        this.ListaCuadrosCargados = null;
        this.CantidadCuadrosCargados = 0;
    }
    #endregion

}
