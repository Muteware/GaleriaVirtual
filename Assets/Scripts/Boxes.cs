﻿using UnityEngine;
using System.Collections;

public class Boxes : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject[] listaCuadros;

    [SerializeField]
    private int posicion;

    [SerializeField]
    private bool esLadoIzquierdo;

    #endregion

    #region PROPIEDADES

    public GameObject[] ListaCuadros
    {
        get
        {
            return listaCuadros;
        }

        set
        {
            listaCuadros = value;
        }
    }

    public int Posicion
    {
        get
        {
            return posicion;
        }

        set
        {
            posicion = value;
        }
    }

    public bool EsLadoIzquierdo
    {
        get
        {
            return esLadoIzquierdo;
        }

        set
        {
            esLadoIzquierdo = value;
        }
    }
    #endregion

    #region METODOS UNITY
    void Start()
    {
    }
    #endregion

    #region METODOS

    #endregion
}
