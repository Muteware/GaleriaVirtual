﻿using UnityEngine;
using System.Collections;

public class GuiUsuario : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject nombreObra;

    [SerializeField]
    private GameObject autorObra;

    [SerializeField]
    private GameObject descripcionObra;

    [SerializeField]
    private GameObject precioObra;

    #endregion

    #region PROPIEDADES
    public GameObject NombreObra
    {
        get
        {
            return nombreObra;
        }

        set
        {
            nombreObra = value;
        }
    }

    public GameObject AutorObra
    {
        get
        {
            return autorObra;
        }

        set
        {
            autorObra = value;
        }
    }

    public GameObject DescripcionObra
    {
        get
        {
            return descripcionObra;
        }

        set
        {
            descripcionObra = value;
        }
    }

    public GameObject PrecioObra
    {
        get
        {
            return precioObra;
        }

        set
        {
            precioObra = value;
        }
    }
    #endregion



}
