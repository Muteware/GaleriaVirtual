﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Usuario : MonoBehaviour {

    #region ATRIBUTOS
    [SerializeField] private GameObject interfazGrafica;
    [SerializeField] private GameObject camara;
    private GuiUsuario datosGui;
    private bool activoAlgunCuadro;
    private GameObject cuadroActivado;
    private RaycastHit posicionDondeMira;
    private GameObject miraUsuario;

    #endregion

    #region PROPIEDADES
    public bool ActivoAlgunCuadro
    {
        get
        {
            return activoAlgunCuadro;
        }

        set
        {
            activoAlgunCuadro = value;
        }
    }

    public GameObject CuadroActivado
    {
        get
        {
            return cuadroActivado;
        }

        set
        {
            cuadroActivado = value;
        }
    }

    public GameObject GUI
    {
        get
        {
            return interfazGrafica;
        }

        set
        {
            interfazGrafica = value;
        }
    }

    public GuiUsuario DatosGui
    {
        get
        {
            return datosGui;
        }

        set
        {
            datosGui = value;
        }
    }

    public RaycastHit PosicionDondeMira
    {
        get
        {
            return posicionDondeMira;
        }

        set
        {
            posicionDondeMira = value;
        }
    }

    public GameObject MiraUsuario
    {
        get
        {
            return miraUsuario;
        }

        set
        {
            miraUsuario = value;
        }
    }

    public GameObject Camara
    {
        get
        {
            return camara;
        }

        set
        {
            camara = value;
        }
    }
    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start()
    {
        this.VisionUsuario();

        this.ActivoAlgunCuadro = false;

        this.DatosGui = this.gameObject.GetComponent<GuiUsuario>();

        this.MiraUsuario = GameObject.Find("MiraUsuario");

    }



    // Update is called once per frame
    void Update()
    {
        //Cuando hago click con el mouse
        if (Input.GetMouseButtonDown(0))
        {
            this.VisionUsuario();
        }

        //Cuando hago click con el mouse derecho
        if(Input.GetMouseButtonDown(1))
        {
            if (this.Camara.GetComponent<Camera>().fieldOfView == 60)
                this.Camara.GetComponent<Camera>().fieldOfView = 30;
            else
                this.Camara.GetComponent<Camera>().fieldOfView = 60;
        }

        this.HoverBotonComprar();

        this.Mira();
    }


    #endregion

    #region METODOS

    /// <summary>
    /// Este metodo va a detectar si el usuario esta viendo un cuadro
    /// </summary>
    private void VisionUsuario()
    {
        RaycastHit hit;

        //Tiro un rayo hacia donde mira la camara
        //Ray ray = Camera.main.ScreenPointToRay(-1*this.transform.forward/*Input.mousePosition*/);

        // Si el rayo tiene un hit
        if (Physics.Raycast(this.Camara.transform.position, this.Camara.transform.forward, out hit))
        {
            // y el hit tiene collider y es un cuadro
            if (hit.collider != null)
            {
                //Debug.Log(hit.collider.gameObject.name);

                //Si selecciona un cuadro
                if (hit.collider.gameObject.CompareTag("Cuadro"))
                {
                    if (hit.collider.gameObject == this.CuadroActivado)
                    {
                        //Si el cuadro es el mismo, desactivo la GUI.
                        this.DesactivarGui();

                        //Indico que no hay uno activado
                        this.ActivoAlgunCuadro = false;

                        //Y pongo el activado en null
                        this.CuadroActivado = null;
                    }
                    else
                    {
                        //Si tengo activado algun GUI de cuadro
                        if (this.ActivoAlgunCuadro)
                        {
                            //Lo desactivo
                            this.DesactivarGui();
                        }

                        //Guardo el cuadro que toque para despues desactivarlo.
                        this.CuadroActivado = hit.collider.gameObject;

                        //Muestro la informacion del cuadro que toque.
                        this.MostrarInformacionCuadro();

                        //Indico que active algun GUI de cuadro
                        this.ActivoAlgunCuadro = true;
                    }
                }
                else if (hit.collider.gameObject.CompareTag("Boton Interno Ascensor"))
                {
                    GameObject.Find("Ascensor").GetComponent<Ascensor>().CambiarPiso(Int32.Parse(hit.collider.gameObject.name));
                }
                else if (hit.collider.name == "triggerBotonComprar")
                {
                    this.CuadroActivado.GetComponent<Cuadro>().AbrirLinkCarrito();
                }
                else
                {
                    //Debug.Log(hit.collider.name);
                }

            }
        }
    }
    /// <summary>
    /// Este metodo va a cambiar el color del boton comprar cuando le paso por arriba con el mouse
    /// </summary>
    private void HoverBotonComprar()
    {
        RaycastHit hit;

        //Tiro un rayo hacia donde mira la camara
        //Ray ray = Camera.main.ScreenPointToRay(-1 * this.transform.forward);

        // Si el rayo tiene un hit
        if (Physics.Raycast(this.Camara.transform.position, this.Camara.transform.forward, out hit))
        {
            this.PosicionDondeMira = hit;

            // y el hit tiene collider y es un cuadro
            if (hit.collider != null)
            {
                if (hit.collider.name == "triggerBotonComprar")
                {
                    //Obtengo el boton
                    GameObject boton = GameObject.Find("Boton Agregar al Carrito");
                    boton.GetComponent<Image>().color = Color.white;
                    this.CuadroActivado.GetComponent<Cuadro>().ActivarTemporizadorBotonCarrito();    
                }
            }
        }
    }

    /// <summary>
    /// Este metodo va a apagar la GUI con la informacion de los cuadros
    /// </summary>
    private void DesactivarGui()
    {
        this.PonerEnBlancoGUI();
        this.CuadroActivado.GetComponent<Cuadro>().CanvasConInformacion.SetActive(false);
        this.GUI.SetActive(false);
    }

    /// <summary>
    /// Este metodo va a mostrar la GUI con la informacion de los cuadros
    /// </summary>
    private void MostrarInformacionCuadro()
    {
        this.GUI.SetActive(true);
        this.CuadroActivado.GetComponent<Cuadro>().CanvasConInformacion.SetActive(true);
        this.CargarGui();
    }

    /// <summary>
    /// Este metodod va a dejar en blanco todos los valores de la GUI
    /// </summary>
    private void PonerEnBlancoGUI()
    {
        this.DatosGui.NombreObra.GetComponent<Text>().text = "";
        this.DatosGui.AutorObra.GetComponent<Text>().text = "";
        this.DatosGui.DescripcionObra.GetComponent<Text>().text = "";
        this.DatosGui.PrecioObra.GetComponent<Text>().text = "";
    }

    /// <summary>
    /// Este metodo va a cargar los valores del cuadro a la GUI
    /// </summary>
    private void CargarGui()
    {
        if(this.CuadroActivado.GetComponent<Cuadro>().Datos != null)
        { 
            this.DatosGui.NombreObra.GetComponent<Text>().text = this.CuadroActivado.GetComponent<Cuadro>().Datos.NombreObra;
            this.DatosGui.AutorObra.GetComponent<Text>().text = this.CuadroActivado.GetComponent<Cuadro>().Datos.AutorObra;
            this.DatosGui.DescripcionObra.GetComponent<Text>().text = this.CuadroActivado.GetComponent<Cuadro>().Datos.DescripcionObra;
            this.DatosGui.PrecioObra.GetComponent<Text>().text = this.CuadroActivado.GetComponent<Cuadro>().Datos.PrecioObra.ToString();
        }
    }

    private void Mira()
    {
        //this.gameObject.GetComponent<LineRenderer>().SetPosition(0, this.gameObject.transform.position);
        //this.gameObject.GetComponent<LineRenderer>().SetPosition(1, this.PosicionDondeMira);

        this.MiraUsuario.transform.position = this.PosicionDondeMira.point;
        this.MiraUsuario.transform.rotation = Quaternion.LookRotation(-this.PosicionDondeMira.normal);
        this.MiraUsuario.transform.Translate(0, 0, -0.1f);

    }
    #endregion
}
