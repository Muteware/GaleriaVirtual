﻿using UnityEngine;
using System.Collections;

public class DatosCuadro {

    #region ATRIBUTOS
    internal int IdObra { set; get; }
    internal string NombreObra { set; get; }
    internal string DescripcionObra { set; get; }
    internal string AutorObra { set; get; }
    internal float PrecioObra { set; get; }
    internal string ImagenLink { set; get; }
    internal float X { set; get; }
    internal float Y { set; get; }
    internal int Piso { set; get; }
    internal int NumeroBox { set; get; }
    internal int NumeroCuadro { set; get; }
    internal string Mail { set; get; }
    internal bool Baja { set; get; }
    #endregion

    #region PROPIEDADES
    #endregion

    #region CONSTRUCTOR

    public DatosCuadro()
    {

    }

    public DatosCuadro(int idObra, string nombreObra, string descripcionObra, string autorObra, float precioObra, string imagenLink, float x, float y, int piso, int numeroBox, int numeroCuadro, string mail, int baja)
    {
        this.IdObra = idObra;
        this.NombreObra = nombreObra;
        this.DescripcionObra = descripcionObra;
        this.AutorObra = autorObra;
        this.PrecioObra = precioObra;
        this.ImagenLink = imagenLink;
        this.X = x;
        this.Y = y;
        this.Piso = piso;
        this.NumeroBox = numeroBox;
        this.NumeroCuadro = numeroCuadro;
        this.Mail = mail;
        this.Baja = (baja == 0) ? false : true;
    }

    #endregion

    #region METODOS

    #endregion
}
