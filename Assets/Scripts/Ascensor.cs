﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Ascensor : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject[] listaBotones;
    [SerializeField]
    private AudioClip[] sonidos;
    /* Abriendo-Ring-Musica-Boton-Subiendo*/


    private int PisoSeleccionado { set; get; }
    private int PisoActual { set; get; }
    private Animator Animaciones { set; get; }
    private GameObject TextoNumeroPiso { set; get; }
    private float TemporizadorAscensor { set; get; }
    private float TiempoEntrePisos { set; get; }
    private int PisosQueMeMuevo { set; get; }
    internal bool PuedoIniciarPuerta { set; get; }
    private AudioSource Audio { set; get; }
    private bool EstaCerrado { set; get; }
    private bool PuseMusica { set; get; }

    #endregion

    #region PROPIEDADES
    public GameObject[] ListaBotones
    {
        get
        {
            return listaBotones;
        }

        set
        {
            listaBotones = value;
        }
    }

    public AudioClip[] Sonidos
    {
        get
        {
            return sonidos;
        }

        set
        {
            sonidos = value;
        }
    }
    #endregion


    #region METODOS UNITY

    // Use this for initialization
    void Start()
    {

        this.PisoSeleccionado = 0;
        this.PisoActual = 0;
        this.Animaciones = this.gameObject.GetComponent<Animator>();
        this.TextoNumeroPiso = GameObject.Find("textoNumeroPiso");
        this.TextoNumeroPiso.GetComponent<Text>().text = this.PisoSeleccionado.ToString();
        this.Audio = this.gameObject.GetComponent<AudioSource>();
        this.EstaCerrado = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.PuedoIniciarPuerta)
        {
            this.AbrirPuertas();
            this.PuedoIniciarPuerta = false;
        }

        this.CuentoSiEstaCerrado();

        if (this.TemporizadorAscensor > 1f && !this.PuseMusica)
        {
            this.PuseMusica = true;
            this.Audio.PlayOneShot(this.Sonidos[2], 0.25f);
        }

        if (this.TemporizadorAscensor > 5f)
        {
            this.AbrirPuertas();
        }
	}

    

    #endregion

    #region METODOS
    /// <summary>
    /// Este metodo se llama cuando se presiona un nuevo boton en el ascensor.
    /// </summary>
    public void CambiarPiso(int numeroPiso)
    {
        this.Audio.PlayOneShot(this.Sonidos[3]);

        if (numeroPiso != this.PisoSeleccionado)
        {

            var pisoAnterior = this.PisoSeleccionado;

            this.PisoSeleccionado = numeroPiso;

            this.CerrarPuertas();

            this.CargarCuadrosNuevoPiso(this.PisoSeleccionado);

            this.PisoActual = this.PisoSeleccionado;

        }
    }

    /// <summary>
    /// Este metodo va a mostrar una aniamcion del cambio de piso
    /// Piso a piso va a ir disminuyendo o aumentando segun corresponda.
    /// </summary>
    private void AnimacionCambioPiso(int pisoAnterior)
    {
        this.Audio.PlayOneShot(this.Sonidos[4], 0.1f);
        this.PisosQueMeMuevo = this.PisoSeleccionado - pisoAnterior;
        this.TiempoEntrePisos = Math.Abs(9f / this.PisosQueMeMuevo);
        this.TemporizadorAscensor = this.TiempoEntrePisos;
        this.Audio.PlayOneShot(this.Sonidos[2], 0.25f);

    }

    /// <summary>
    /// Este metodo va a cambiar el texto 
    /// </summary>
    private void CambiarTextoNumeroPiso()
    {
        this.TextoNumeroPiso.GetComponent<Text>().text = this.PisoActual.ToString();
    }

    private void CargarCuadrosNuevoPiso(int piso)
    {
        var cuadroManager = GameObject.Find("CuadrosManager");
        cuadroManager.GetComponent<LectorJSONv2>().TengoQueProcesar = true;
        cuadroManager.GetComponent<LectorJSONv2>().PisoParaCargar = piso;
        cuadroManager.GetComponent<CuadrosManager>().ReiniciarCanvasCuadros();
    }

    /// <summary>
    /// Este metodo va a ejecutar la animacion de las puertas abriendose.
    /// </summary>
    private void AbrirPuertas()
    {
        this.CambiarTextoNumeroPiso();
        this.EstaCerrado = false;
        this.Audio.Stop();
        this.Audio.PlayOneShot(this.Sonidos[1]);
        this.Animaciones.SetBool("abrir", true);
        this.Animaciones.SetBool("cerrar", false);
        this.Audio.PlayOneShot(this.Sonidos[0],0.5f);
        
    }

    /// <summary>
    /// Este metodo va a ejecutar la animacion de las puertas cerrandose.
    /// </summary>
    private void CerrarPuertas()
    {
        this.Audio.PlayOneShot(this.Sonidos[0],0.5f);
        this.Animaciones.SetBool("abrir", false);
        this.Animaciones.SetBool("cerrar", true);
        this.TemporizadorAscensor = this.TiempoEntrePisos;
        this.EstaCerrado = true;
        this.PuseMusica = false;
        
    }

    /// <summary>
    /// Este metodo va a chequear cuanto tiempo esta cerrada la puerta del ascensor
    /// </summary>
    private void CuentoSiEstaCerrado()
    {
        if (this.EstaCerrado)
        {
            this.TemporizadorAscensor += Time.deltaTime;
        }
        else
        {
            this.TemporizadorAscensor = 0;
        }
    }
    #endregion

    #region BOTONES

    #endregion

}
