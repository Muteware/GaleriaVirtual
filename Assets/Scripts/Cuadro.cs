﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System;

public class Cuadro : MonoBehaviour
{

    #region ATRIBUTOS

    [SerializeField] private GameObject canvasConInformacion;
    [SerializeField] private GameObject botonAgregarCarrito;
    private DatosCuadro datos;
    private float tiempoHastaPonerNegroElBoton;
    private float temporizadorPonerNegroBoton;
    private bool meEstaViendoElUsuario;
    private Texture2D nuevaTextura;

    public bool PuedoCargarTextura { set; get; }
    public bool PuedoCargarImagen { set; get; }

    #endregion

    #region PROPIEDADES
    public DatosCuadro Datos
    {
        get
        {
            return datos;
        }

        set
        {
            datos = value;
        }
    }

    public float TemporizadorPonerNegroBoton
    {
        get
        {
            return temporizadorPonerNegroBoton;
        }

        set
        {
            temporizadorPonerNegroBoton = value;
        }
    }

    public float TiempoHastaPonerNegroElBoton
    {
        get
        {
            return tiempoHastaPonerNegroElBoton;
        }

        set
        {
            tiempoHastaPonerNegroElBoton = value;
        }
    }

    public bool MeEstaViendoElUsuario
    {
        get
        {
            return MeEstaViendoElUsuario;
        }

        set
        {
            MeEstaViendoElUsuario = value;
        }
    }

    public GameObject CanvasConInformacion
    {
        get
        {
            return canvasConInformacion;
        }

        set
        {
            canvasConInformacion = value;
        }
    }

    public GameObject BotonAgregarCarrito
    {
        get
        {
            return botonAgregarCarrito;
        }

        set
        {
            botonAgregarCarrito = value;
        }
    }

    public Texture2D NuevaTextura
    {
        get
        {
            return nuevaTextura;
        }

        set
        {
            nuevaTextura = value;
        }
    }
    #endregion

    #region METODOS UNITY

    // Use this for initialization
    void Start()
    {
        this.NuevaTextura = new Texture2D(1, 1);//256x256
        this.TiempoHastaPonerNegroElBoton = 0.2f;
        this.PuedoCargarTextura = false;
        this.PuedoCargarImagen = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.CanvasConInformacion.activeSelf)
        {
            if (this.TemporizadorPonerNegroBoton > 0)
                this.TemporizadorPonerNegroBoton -= Time.deltaTime;
            else
                this.BotonAgregarCarrito.GetComponent<Image>().color = Color.black;
        }

        if(this.PuedoCargarTextura)
        {
            this.PuedoCargarTextura = false;
            this.IniciarCargaImagen();
            this.ReestablecerMesh(GameObject.Find("CuadrosManager").GetComponent<CuadrosManager>().MeshCuadro);
            this.PuedoCargarImagen = true;
        }

        if (this.PuedoCargarImagen)
        {
            this.PuedoCargarImagen = false;
            this.CargarImagenDescargada(this.NuevaTextura);
        }
    }
    #endregion

    #region METODOS

    
    /// <summary>
    /// Este metodo va a cargar la imagen correspondiente al cuadro desde sus datos
    /// </summary>
    IEnumerator CargarImagen()
    {
        var www = new WWW(this.Datos.ImagenLink);

        yield return www;

        //Si no devuelve error.
        if (www.error == null)
        {
            // Asigno la imagen descargada en la textura que va a usar el objeto
            www.LoadImageIntoTexture(this.NuevaTextura);
            
        }

        www.Dispose();
        www = null;
    }

    /// <summary>
    /// Este metodo se llama cuando el usuario le hace click al cuadro
    /// Va a mostrar la informacion del mismo en un Canvas al lado
    /// </summary>
    public void MostrarInformacion()
    {
        this.CanvasConInformacion.SetActive(true);
    }

    /// <summary>
    /// Este metodo va a ocultar la informacion del cuadro si el usuario no la esta mirando
    /// por 5 segundos
    /// </summary>
    public void OcultarInformacion()
    {
        this.CanvasConInformacion.SetActive(false);
    }

    /// <summary>
    /// Este metodo va a activar el temporizador del boton del carrito para que este en blanco y pase a negro
    /// Lo llama el Usuario cuando lo mira por eso es publico
    /// </summary>
    public void ActivarTemporizadorBotonCarrito()
    {
        this.TemporizadorPonerNegroBoton = this.TiempoHastaPonerNegroElBoton;
    }

    /// <summary>
    /// Este metodo se utiliza para guardar los datos del cuadro leido por el JSON
    /// </summary>
    public void GuardarDatos(DatosCuadro datosDelCuadro)
    {
        //Guardo los datos del cuadro
        this.Datos = datosDelCuadro;
    }

    /// <summary>
    /// Este metodo va a iniciar la corrutina para que se cargue la imagen del cuadro
    /// </summary>
    public void IniciarCargaImagen()
    {
        StartCoroutine(this.CargarImagen());
    }

    /// <summary>
    /// Este metodo carga la imagen descargada en el material del objeto
    /// </summary>
    /// <param name="nuevaTextura"></param>
    public void CargarImagenDescargada(Texture2D nuevaTextura)
    {
        this.gameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", nuevaTextura);
    }

    public void RedimensionarCuadro()
    {
        //Genero una nueva escala para el cuadro con los datos. Vector3(profundidad,alto,largo). El alto va dividio por 3 por la escala que se maneja
        Vector3 escalaNueva = new Vector3(0.05f, (this.Datos.Y/3), this.Datos.X);
        this.transform.localScale = escalaNueva;
        Debug.Log(this.transform.localScale);
        //Me fijo si lo tengo que mostrar
        if (this.Datos.Baja)
        { 
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        else
        { 
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            this.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }

    public void ReestablecerMesh(Mesh nuevaMesh)
    {
        this.gameObject.GetComponent<MeshFilter>().mesh = nuevaMesh;
        this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
    }

    private void CargarDatosPrueba()
    {
        this.Datos = new DatosCuadro();
        this.Datos.NombreObra = "Joker";
        this.Datos.AutorObra = "Bruno Diaz";
        this.Datos.DescripcionObra = "En secreto es Batman";
        this.Datos.PrecioObra = 1231;
    }

    #endregion

    #region BOTONES

    /// <summary>
    /// Este metodo va a abrir una nueva pestania en el navegador al link del carrito
    /// </summary>
    public void AbrirLinkCarrito()
    {
        var linkCarrito = GameObject.Find("CuadrosManager").GetComponent<CuadrosManager>().LinkCarrito + "?id=" + this.Datos.IdObra;
#if !UNITY_EDITOR
		openWindow(linkCarrito);
#endif
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
    #endregion

}
